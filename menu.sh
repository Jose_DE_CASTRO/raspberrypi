#!/bin/bash

### =================================================================
### TITRE : Assistant d installation de Raspbian sur Raspberry-Pi
### CONTEXTE : RASPBERRY-PI / RASPBIAN / DOMOTICZ
### AUTEUR : José De Castro
### DATE : lundi 18 mai 2020
### REVISION : 1.2
### MODIFICATION : dimanche 03 avril 2022
URL GitLab : https://gitlab.com/Jose_DE_CASTRO/raspberrypi/-/blob/main/menu.sh
### =================================================================

### =================================================================
### Variables prédéfinies
### =================================================================

### URL de telechargement de Raspbian
url_raspbian='https://downloads.raspberrypi.org/raspbian_lite_latest'

### nom de fichier arbitraire pour le téléchargement de l'archive Raspbian
raspbian_zip='raspbian-lite.zip'

### Nom de l'image RASPBIAN (.img ou .iso)
ImageRaspbian="raspbian-lite.img"

### Largeur des boites de dialogue
largeurDialogBox=74

### Couleurs
noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rougeclair='\e[0;91m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
jaunefonce='\e[0;33m'
jauneclair='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'
neutre='\e[0;m'

### Choix & Libellé des choix
CHOIX01="MAJ"; libCHOIX01="Mise a jour du systeme"; execCHOIX01=""; Defaut01="ON" 
CHOIX011="WIFI"; libCHOIX011="Activation du Wifi "; execCHOIX011=""; Defaut011="OFF" 
CHOIX02="WEBMIN"; libCHOIX02="Administration du système en mode WEB "; execCHOIX02=""; Defaut02="OFF" 
CHOIX03="APACHE"; libCHOIX03="Serveur web Apache2	"; execCHOIX03=""; Defaut03="OFF" 
CHOIX04="FAIL2BAN"; libCHOIX04="Protection par bannissement	"; execCHOIX04=""; Defaut04="OFF" 
CHOIX05="DOMOTICZ"; libCHOIX05="Logiciel de domotique	"; execCHOIX05=""; Defaut05="OFF" 
CHOIX06="WIRINGPI"; libCHOIX06="Gestion des ports GPIO	"; execCHOIX06=""; Defaut06="OFF" 
CHOIX07="MotionEYE"; libCHOIX07="Système de vidéosurveillance	"; execCHOIX07=""; Defaut07="OFF" 
CHOIX08="QUITTER"; libCHOIX08="Quitter cet assistant                          "; execCHOIX08=""; Defaut08="OFF" 

### Memoriser les libellés originels (certaines libellés de menu seront modifiés)
libOriginelChOIX01=$libCHOIX01
libOriginelChOIX011=$libCHOIX011
libOriginelChOIX02=$libCHOIX02
libOriginelChOIX03=$libCHOIX03
libOriginelChOIX04=$libCHOIX04
libOriginelChOIX05=$libCHOIX05
libOriginelChOIX06=$libCHOIX06
libOriginelChOIX07=$libCHOIX07
libOriginelChOIX08=$libCHOIX08

### Activer le menu principal
MenuPrincipal=true

### Passerelle par defaut
PASSERELLE_IP=$(ip route show default | awk '/default/ {print $3}')
echo $PASSERELLE_IP

### Adresse IP via carte active par défaut
ADRESSE_IP=$(ip route get 1.2.3.4 | awk '{print $7}')
echo $ADRESSE_IP

### =================================================================
### Test si l'utilisateur courant a les droits administrateur
### =================================================================

utilisateur=$(whoami)
if [ "$utilisateur" != "root" ]; then
    printf "${jauneclair} \nCher $utilisateur, vous n'êtes pas 'root'; merci de relancer cette commande précédée de 'SUDO'. \n ${neutre}"
    exit
else
    clear
fi

### =================================================================
### Déclaration des fonctions
### =================================================================

### Rappel : pour inclure un fichier
###  `source FILE" ou ". FILE`
### Exemple : `source $(dirname "$0")/fichier.shl
### =======================

function PartitionMontee() {
# Teste si la partition $1 du périphérique $2 est montée
# Entrée : $1 nom de la partition
# Entrée : $2 périphérique support de ladite partition
# Sortie : le point de montage de la partition ou chaine vide si échec
vlPartition=$1; vlSupport=$2

resultat=""; # Sortie par défaut
if [ "$vlSupport" = "" ]; then
	# Pas de périphérique/support !
	Texte="Aucun périphérique défini"; Niveau=1; RetourLigne=1; MsgErreur=1
	MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
	resultat=""
else
	# Récupérer le point de montage
	vlPointMontage=$(cat /etc/mtab | grep "/dev/$vlSupport" | grep -i "$vlPartition" | cut -d' ' -f2)
	if [ "$vlPointMontage" = "" ]; then
		### Message : Problème, le point de montage est vide
		Texte="Partition non montée"; Niveau=1; RetourLigne=1; MsgErreur=1
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
	else
		resultat=$vlPointMontage
	fi
fi
# Sortie
echo $resultat;
}


function PaquetPresent() {
# Le paquet $1 est il installé ?
# $1 Nom du paquet à tester
# Sortie : 0=paquet pas installé; 1=paquet installé
paquet=$1
resultat=$(dpkg -l | grep $paquet >/dev/null && echo 1 || echo 0)
echo $resultat; return $resultat
}

function MsgConsole() { 
# $1 texte à afficher
# $2 niveau "hiérarchique" (indentation)
# $3 retour à la ligne ? (0=non, 1=oui, 0 par défaut)
# $4 type message ? (0=normal, 1=erreur, 0 par défaut)

# $1 texte à afficher
Texte=$1

# $2 niveau "hiérarchique" (indentation)
Niveau=$2

#$3 retour à la ligne ? (1=oui, par défaut / autre=)
if [[ $3 = 1 ]]; then
	EOL="\n${neutre}"
else
	EOL="${neutre}"
fi

#$4 type message ? (0=normal, 1=erreur, 0 par défaut)
if [[ $4 = 1 ]]; then
	TypeMessage="\e[1;97;41m"
else
	TypeMessage=""
fi

case $Niveau in
	0)
		Style="\e[0;1;33;49m\n";;
		
	1)
		Style="\e[0;1;4;96;49m\n\t";;
		
	2)
		Style="\e[0;1;97;49m\n\t\t* ";;
		
	3) 
		Style="\e[0;97;49m\n\t\t\t- ";;
		
	4) 
		Style="\e[0;37;49m\n\t\t\t\t";;
esac

### Afficher le texte
printf "$Style$TypeMessage$Texte$EOL"
return $? 
}
### =======================



### =================================================================
### Choix des options d'installation
### =================================================================

### ----------
### Rappel : 
### &0 - stdin
### &1 - stdout
### &2 - stderr
### &n - sortie virtuelle (pour faire une permutation par exemple, comme dans le cas ci-dessous où n vaut 3)
### So each of these numbers in your command refer to a file descriptor. You can either redirect a file descriptor to a file with > or redirect it to another file descriptor with >&
### The 3>&1 in your command line will create a new file descriptor and redirect it to 1 which is STDOUT. Now 1>&2 will redirect the file descriptor 1 to STDERR and 2>&3 will redirect file descriptor 2 to 3 which is STDOUT.
### So basically you switched STDOUT and STDERR, these are the steps:
###    1) Create a new fd 3 and point it to the fd 1
###    2) Redirect file descriptor 1 to file descriptor 2. If we wouldn't have saved the file descriptor in 3 we would lose the target.
###    3) Redirect file descriptor 2 to file descriptor 3. Now file descriptors one and two are switched.
### Now if the program prints something to the file descriptor 1, it will be printed to the file descriptor 2 and vice versa.
### ----------

while $MenuPrincipal; do
	CHOIX=$(whiptail --title "Menu d'installation du RaspberryPi" --checklist \
		"\nFonctionnalités proposées :" 16 $largeurDialogBox 8 \
		"$CHOIX01" ": $libCHOIX01" "$Defaut01" \
		"$CHOIX02" ": $libCHOIX02" "$Defaut02" \
		"$CHOIX03" ": $libCHOIX03" "$Defaut03" \
		"$CHOIX04" ": $libCHOIX04" "$Defaut04" \
		"$CHOIX05" ": $libCHOIX05" "$Defaut05" \
		"$CHOIX06" ": $libCHOIX06" "$Defaut06" \
		"$CHOIX07" ": $libCHOIX07" "$Defaut07" \
		"$CHOIX08" ": $libCHOIX08" "$Defaut08" 3>&1 1>&2 2>&3)
	exitstatus=$?
	if [[ ! $exitstatus = 0 ]]; then
		### Il faut quitter le menu
		MenuPrincipal=false
	fi
	
	if [[ $exitstatus = 0 ]]; then
		#clear
		### Analyser le/les choix
		Texte="=================================================================================="; Niveau=0; RetourLigne=0; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
		Texte="Fonctionnalités à installer :"; Niveau=0; RetourLigne=0; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		Texte="=================================================================================="; Niveau=0; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
	fi
	
	if [[ $CHOIX =~ "$CHOIX01" ]]; then # MAJ
		Texte="..::: $CHOIX01 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		titreBOX="$libOriginelChOIX01" # Titre des boites de dialogue
		
		### Par défaut c'est OK...
		if [ "$execCHOIX01" = "" ]; then
			execCHOIX01="OK"; Defaut01="OFF"; libCHOIX01="[OK] $libOriginelChOIX01";
		fi
		
		Texte="Mise à jour des paquets"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		apt update -y && apt upgrade -y

		if [[ $(PaquetPresent "aptitude") = 1 ]]; then
			Texte="Paquet 'aptitude' déjà installé"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		else		
			Texte="Installation du paquet 'aptitude' (gestionnaire de paquets amélioré)"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt install -y aptitude
		fi
	
		if [[ $(PaquetPresent "wget") = 1 ]]; then
			Texte="Paquet 'wget' déjà installé"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		else
			Texte="Installation du paquet 'wget' (téléchargement de fichiers)"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt install -y wget
		fi
	
		#Texte="Installation du paquet 'git', si nécessaire"; Niveau=2; RetourLigne=1; MsgErreur=0
		#MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		#apt install -y git
	
		if [[ $(PaquetPresent "mc") = 1 ]]; then
			Texte="Paquet 'mc' déjà installé"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		else
			Texte="Installation du paquet 'mc' (gestionnaire de fichiers / editeur de texte)"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt install -y mc
		fi
	
		if [[ $(PaquetPresent "locate") = 1 ]]; then
			Texte="Paquet 'locate' déjà installé"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		else
			Texte="Installation du paquet 'locate' (indexation des fichiers)"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt install -y locate && updatedb
		fi
	
		Texte="Synchronisation de l'horloge / du temps"; Niveau=2; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
		if [[ $(PaquetPresent "ntp") = 1 ]]; then
			Texte="Paquet 'ntp' déjà installé"; Niveau=3; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		else		 
			Texte="Installation du paquet 'ntp', si nécessaire"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
			aptitude install -y ntp
		fi
		Texte="Démarrage du service 'ntp', si nécessaire"; Niveau=3; RetourLigne=0; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		/etc/init.d/ntp start
		Texte="Configuration de 'ntp'"; Niveau=3; RetourLigne=0; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		if [ -e /etc/ntp.com ] ; then
			Texte="Le fichier /etc/ntp.com existe déjà"; Niveau=4; RetourLigne=0; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
			Texte="Suppression du fichier existant"; Niveau=4; RetourLigne=0; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
			rm -f /etc/ntp.com
		fi
		Texte="Création du fichier de configuration"; Niveau=4; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		echo "server 0.fr.pool.ntp.org" >/dev/null | sudo tee -a /etc/ntp.com >/dev/null

		### Saut de ligne
		printf "\n";
	fi	
	if [[ $CHOIX =~ "$CHOIX011" ]]; then # WIFI
		Texte="..::: $CHOIX011 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		titreBOX="$libOriginelChOIX011" # Titre des boites de dialogue
		### Par défaut c'est OK...
		if [ "$execCHOIX011" == "" ]; then
			execCHOIX011="OK"; Defaut011="OFF"; libCHOIX011="[OK] $libOriginelChOIX011";
		fi
		
		# Déblocage des fonctions et activation de l'interface Wifi
		rfkill unblock wifi
		#rfkill unblock all
		ifconfig wlan0 up	
		#
	fi
	if [[ $CHOIX =~ "$CHOIX02" ]]; then # WEBMIN
		Texte="..::: $CHOIX02 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		titreBOX="$libOriginelChOIX02" # Titre des boites de dialogue
		### Par défaut c'est OK...
		if [ "$execCHOIX02" = "" ]; then
			execCHOIX02="OK"; Defaut02="OFF"; libCHOIX02="[OK] $libOriginelChOIX02";
		fi
		
		# Le programme est il déjà installé ?    	
		if [[ $(PaquetPresent "webmin") = 1 ]]; then
			# Paquet déjà installé, il faut purger l'installation précédente
			Texte="Webmin est déja installé. Désinstallation avec purge de l'installation précédente"; Niveau=2; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt purge -y webmin	
		fi	
		Texte="Installation de la derniere version de WEBMIN"; Niveau=2; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			Texte="Installation des dépendances"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			aptitude install -y libnet-ssleay-perl openssl libauthen-pam-perl libio-pty-perl apt-show-versions zip
			
			Texte="Telechargement de la derniere version de Webmin"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			#wget -q --show-progress http://www.webmin.com/download/deb/webmin-current.deb --no-check-certificate
			tmpurl="http://www.webmin.com/download/deb/webmin-current.deb"
			tmpfile="webmin-current.deb"
			#wget --progress=dot -O "$tmpfile" "$tmpurl" --no-check-certificate 2>&1 | grep "%" | sed -u -e "s,\.,,g" | awk '{print $2}' | sed -u -e 's/[%]//' | whiptail --title "Téléchargement de WEBMIN" --gauge "" 6 50 0
			wget --progress=dot -O "$tmpfile" "$tmpurl" --no-check-certificate 2>&1 | stdbuf -o0 awk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3) }' | whiptail --title "Téléchargement de WEBMIN" --gauge "" 6 50 0
			
			Texte="Installer le paquet puis supprimer l'archive"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			dpkg --install $tmpfile && rm -f $tmpfile		

		### Saut de ligne
		printf "\n";
	fi	
	if [[ $CHOIX =~ "$CHOIX03" ]]; then # APACHE
		Texte="..::: $CHOIX03 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		titreBOX="$libOriginelChOIX03" # Titre des boites de dialogue
		### Par défaut c'est OK...
		if [ "$execCHOIX03" == "" ]; then
			execCHOIX03="OK"; Defaut03="OFF"; libCHOIX03="[OK] $libOriginelChOIX03";
		fi
		
		Texte="Installation de la derniere version de APACHE2"; Niveau=2; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		# Le programme est il déjà installé ?    	
		if [[ $(PaquetPresent "apache2") == 1 ]]; then
			# Paquet déjà installé, il faut purger l'installation précédente
			Texte="APACHE2 est déja installé. Désinstallation avec purge de l'installation précédente"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			apt purge -y apache2	
			if [ -d "/var/www/passwd" ]; then
        		Texte="Suppression du répertoire des mots de passe existant."; Niveau=3; RetourLigne=1; MsgErreur=0
				MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
				rm -r /var/www/passwd
	       fi
		fi
		Texte="Installation du méta-paquet 'apache2'"; Niveau=3; RetourLigne=1; MsgErreur=0
		MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
		apt install -y apache2
		
		if [ -f "/var/www/html/index.html" ]; then
			Texte="Suppression de la page par défaut '/var/www/html/index.html' livrée par 'apache2'"; Niveau=3; RetourLigne=1; MsgErreur=0
			MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
			rm -f /var/www/html/index.html
		fi
	
		### Saut de ligne
		printf "\n";
	fi	
	if [[ $CHOIX =~ "$CHOIX04" ]]; then # FAIL2BAN
		printf "${cyanclair}\n\t$CHOIX04 : ${neutre}"
		titreBOX="$libOriginelChOIX04"
		printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
		### Par défaut c'est OK...
		if [ "$execCHOIX04" == "" ]; then
			execCHOIX04="OK"; Defaut04="OFF"; libCHOIX04="[OK] $libOriginelChOIX04";
		fi
		### Saut de ligne
		printf "\n";
	fi	
	if [[ $CHOIX =~ "$CHOIX05" ]]; then # DOMOTICZ
		printf "${cyanclair}\n\t$CHOIX05 : ${neutre}"
		titreBOX="$libOriginelChOIX05"
		printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
		### Par défaut c'est OK...
		if [ "$execCHOIX05" = "" ]; then
			execCHOIX05="OK"; Defaut05="OFF"; libCHOIX05="[OK] $libOriginelChOIX05";
		fi
		### Saut de ligne
		printf "\n";	
	fi	
	if [[ $CHOIX =~ "$CHOIX06" ]]; then # WIRINGPI
		printf "${cyanclair}\n\t$CHOIX06 : ${neutre}"
		titreBOX="$libOriginelChOIX06"
		printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
		### Par défaut c'est OK...
		if [ "$execCHOIX06" == "" ]; then
			execCHOIX06="OK"; Defaut06="OFF"; libCHOIX06="[OK] $libOriginelChOIX06";
		fi
		### Saut de ligne
		printf "\n";
	fi	
	if [[ $CHOIX =~ "$CHOIX07" ]]; then # MOTIONEYE
		printf "${cyanclair}\n\t$CHOIX07 : ${neutre}"
		titreBOX="$libOriginelChOIX07"
		printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
		### Par défaut c'est OK...
		if [ "$execCHOIX07" == "" ]; then
			execCHOIX07="OK"; Defaut07="OFF"; libCHOIX07="[OK] $libOriginelChOIX07";
		fi
		### Saut de ligne
		printf "\n";
	fi						
	if [[ $CHOIX =~ "$CHOIX08" ]]; then
		printf "${cyanclair}\n\t$CHOIX08 : ${neutre}"
		titreBOX="$libOriginelChOIX08"
		printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
		### Par défaut c'est OK...
		if [ "$execCHOIX08" == "" ]; then
			execCHOIX08="OK"; Defaut08="OFF"; libCHOIX08="[OK] $libOriginelChOIX08";
		fi
			
		### Quitter le menu !			
		MenuPrincipal=false
			
		### Saut de ligne
		printf "\n";
	fi
done

### Sortie 
if [ ! $QuitterMenu == 0 ]; then
	printf "${jauneclair}\nAu revoir.\n${neutre}"
	exit 0
else
	### Sortie non gérée du programme
	exit 1
fi
