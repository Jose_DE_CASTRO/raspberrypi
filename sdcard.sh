#!/bin/bash

### =================================================================
### TITRE : Assistant d installation de Raspbian sur Raspberry-Pi
### CONTEXTE : RASPBERRY-PI / RASPBIAN / INSTALLATION
### AUTEUR : José De Castro
### REVISION : 1.1
### DATE : lundi 18 mai 2020
### MODIFICATION : dimanche 30 janvier 2022
### URL GitLab : https://gitlab.com/Jose_DE_CASTRO/raspberrypi/-/blob/main/sdcard.sh
### =================================================================

### =================================================================
### Variables prédéfinies
### =================================================================

### URL de telechargement de Raspbian
url_raspbian='https://downloads.raspberrypi.org/raspbian_lite_latest'

### URLs du script post-installation 
url_fic_install='https://gitlab.com/Jose_DE_CASTRO/raspberrypi/-/raw/main/menu.sh'

### nom de fichier arbitraire pour le téléchargement de l'archive Raspbian
raspbian_zip='raspbian-lite.zip'

### Nom de l'image RASPBIAN (.img ou .iso)
ImageRaspbian="raspbian-lite.img"

### Largeur des boites de dialogue
largeurDialogBox=74

### Couleurs
noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rougeclair='\e[;31m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
jaunefonce='\e[0;33m'
jauneclair='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'
neutre='\e[0;m'


### =================================================================
### Déclaration des fonctions
### =================================================================

### Rappel : pour inclure un fichier
###  `source FILE" ou ". FILE`
### Exemple : `source $(dirname "$0")/fichier.shl
### =======================


function MsgConsole() { 
# $1 texte à afficher
# $2 niveau "hiérarchique" (indentation)
#$3 retour à la ligne ? (0=non, 1=oui, 0 par défaut)
#$4 type message ? (0=normal, 1=erreur, 0 par défaut)

# $1 texte à afficher
Texte=$1

# $2 niveau "hiérarchique" (indentation)
Niveau=$2

#$3 retour à la ligne ? (1=oui, par défaut / autre=)
if [[ $3 = 1 ]]; then
	EOL="\n${neutre}"
else
	EOL="${neutre}"
fi

#$4 type message ? (0=normal, 1=erreur, 0 par défaut)
if [[ $4 = 1 ]]; then
	TypeMessage="\e[1;97;41m"
else
	TypeMessage=""
fi

case $Niveau in
	0)
		Style="\e[0;1;33;49m\n";;
		
	1)
		Style="\e[0;1;4;96;49m\n\t";;
		
	2)
		Style="\e[0;1;97;49m\n\t\t* ";;
		
	3) 
		Style="\e[0;97;49m\n\t\t\t- ";;
		
	4) 
		Style="\e[0;37;49m\n\t\t\t\t";;
esac

### Afficher le texte
printf "$Style$TypeMessage$Texte$EOL" 
return $? 
}
### =======================

function PartitionMontee() {
# Teste si la partition $1 du périphérique $2 est montée
# Entrée : $1 nom de la partition
# Entrée : $2 périphérique support de ladite partition
# Utilise $largeurDialogBox (variable globale pour largeur boite de dialogue)
# Sortie : $3 contient le point de montage de la partition ou chaine vide si échec
# Entrée : $4 si ce paramètre existe alors message écran (en plus du message stdout)
local vlPartition=$1; local vlSupport=$2; vlTitreBOX=$4; local resultat=""; local Texte=""

resultat=""; # Sortie par défaut
if [ "$vlSupport" = "" ]; then
	# Pas de périphérique/support !
	Texte="Aucun périphérique défini"; Niveau=1; RetourLigne=1; MsgErreur=1; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";  
	resultat=""
else
	# Récupérer le point de montage
	vlPointMontage=$(cat /etc/mtab | grep "/dev/$vlSupport" | grep -i "$vlPartition" | cut -d' ' -f2)
	if [ "$vlPointMontage" = "" ]; then
		### Message : Problème, le point de montage est vide
		Texte="Partition $vlPartition non montée"; Niveau=1; RetourLigne=1; MsgErreur=1; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		resultat="" 
	elif [ "$vlPointMontage" = "/" ]; then
		### Message : Problème, le point de montage est sur la racine
		Texte="Anormal : partition montée sur la racine '/'."; Niveau=1; RetourLigne=1; MsgErreur=1; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
		resultat="" 
	else
		### OK
		resultat=$vlPointMontage
	fi
fi
# Message écran ?
if [ ! "$vlTitreBOX" = "" ] && [ ! "$Texte" = "" ]; then
	whiptail --title "$vlTitreBOX" --msgbox "$Texte" 10 $largeurDialogBox
fi
# Sortie
eval $3="$resultat"
}

### Choix & Libellé des choix
CHOIX01="TELECHARGEMENT"; libCHOIX01="Téléchargement de Raspbian"; execCHOIX01=""; 
CHOIX02="SUPPORT"; libCHOIX02="Choix du support pour l installation"; execCHOIX02="";
CHOIX03="COPIE"; libCHOIX03="Copie de Raspbian sur le support choisi"; execCHOIX03="";
CHOIX04="NOMMAGE"; libCHOIX04="Nommage du Raspbian/RaspberryPi"; execCHOIX04="";
CHOIX05="SSH"; libCHOIX05="Activation du SSH dans RASPBIAN"; execCHOIX05="";
CHOIX06="WIFI"; libCHOIX06="Configuration du Wifi dans Raspbian"; execCHOIX06="";
CHOIX07="SCRIPTS"; libCHOIX07="Copier le(s) script(s) sur le périphérique"; execCHOIX07="";
CHOIX08="DEMONTER"; libCHOIX08="Démonter le périphérique"; execCHOIX08="";
CHOIX09="QUITTER"; libCHOIX09="Quitter cet assistant"; execCHOIX09="";

### Memoriser les libellés originels (certaines libellés de menu seront modifiés)
libOriginelChOIX01=$libCHOIX01
libOriginelChOIX02=$libCHOIX02
libOriginelChOIX03=$libCHOIX03
libOriginelChOIX04=$libCHOIX04
libOriginelChOIX05=$libCHOIX05
libOriginelChOIX06=$libCHOIX06
libOriginelChOIX07=$libCHOIX07
libOriginelChOIX08=$libCHOIX08
libOriginelChOIX09=$libCHOIX09

### Activer le menu principal
menuprincipal=true

### Passerelle par defaut
PASSERELLE_IP=$(ip route show default | awk '/default/ {print $3}')
#echo ${PASSERELLE_IP}

### =================================================================
### Test si l'utilisateur courant a les droits administrateur
### =================================================================

utilisateur=$(whoami)
if [ "$utilisateur" != "root" ]; then
    printf "${jauneclair} \nCher $utilisateur, vous n'êtes pas 'root'; merci de relancer cette commande précédée de 'sudo'. \n\n ${neutre}"
    exit
else
    clear
fi

### =================================================================
### Choix des options d'installation
### =================================================================

### ----------
### Rappel : 
### 0 - stdin
### 1 - stdout
### 2 - stderr
### So each of these numbers in your command refer to a file descriptor. You can either redirect a file descriptor to a file with > or redirect it to another file descriptor with >&
### The 3>&1 in your command line will create a new file descriptor and redirect it to 1 which is STDOUT. Now 1>&2 will redirect the file descriptor 1 to STDERR and 2>&3 will redirect file descriptor 2 to 3 which is STDOUT.
### So basically you switched STDOUT and STDERR, these are the steps:
###    1) Create a new fd 3 and point it to the fd 1
###    2) Redirect file descriptor 1 to file descriptor 2. If we wouldn't have saved the file descriptor in 3 we would lose the target.
###    3) Redirect file descriptor 2 to file descriptor 3. Now file descriptors one and two are switched.
### Now if the program prints something to the file descriptor 1, it will be printed to the file descriptor 2 and vice versa.
### ----------

while $menuprincipal; do
	CHOIX=$(whiptail --title "Assistant d'installation de RASPBIAN sur RaspberryPi" --menu \
		"\nEtapes proposées :" 17 $largeurDialogBox 9 \
		"$CHOIX01" ": $libCHOIX01" \
		"$CHOIX02" ": $libCHOIX02" \
		"$CHOIX03" ": $libCHOIX03" \
		"$CHOIX04" ": $libCHOIX04" \
		"$CHOIX05" ": $libCHOIX05" \
		"$CHOIX06" ": $libCHOIX06" \
		"$CHOIX07" ": $libCHOIX07" \
		"$CHOIX08" ": $libCHOIX08" \
		"$CHOIX09" ": $libCHOIX09" 3>&1 1>&2 2>&3)
		exitstatus=$?
	
	### Analyser le/les choix
	#printf "${jauneclair}  ======================================= \n ${neutre}"
	#printf "${jauneclair} Action a realiser : ${bleuclair}$CHOIX \n ${neutre}"
	#printf "${jauneclair} ======================================= \n ${neutre}"
		
	case $CHOIX	in	
		$CHOIX01)
			### =======================================
			### PREPARATION et TELECHARGEMENT
			Texte="..::: $CHOIX01 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX01"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			### Par défaut c'est OK...
			if [ "$execCHOIX01" = "" ]; then
				execCHOIX01="OK";	libCHOIX01="[OK] Téléchargement effectué"
			fi
			
			### Suppression des (éventuels) anciens fichiers
			printf "${bleuclair}\n\t\t* Suppression des (éventuels) anciens fichiers \".IMG\" et \".ZIP\"  ${neutre}"
			rm -f 20*raspbian*lite.img 
			rm -f "$ImageRaspbian"
			rm -f $raspbian_zip*.zip
			
			### Télécharger la dernière version de Raspbian lite (et renommer le fichier raspbian.zip) : 
			printf "${bleuclair}\n\t\t* Téléchargement de la dernière version de Raspbian  ${neutre}"
			wget --progress=dot -O $raspbian_zip "$url_raspbian" 2>&1 | grep "%" | sed -u -e "s,\.,,g" | awk '{print $2}' | sed -u 's/[%]//' | whiptail --title "Téléchargement de RASPBIAN" --gauge "" 6 50 0
			
			### Décompacter le fichier archive
			printf "${bleuclair}\n\t\t* Décompactage du fichier \".ZIP\" téléchargé  ${neutre}"
			unzip $raspbian_zip >/dev/null
			
			### Supprimer le fichier archive
			printf "${bleuclair}\n\t\t* Suppression du fichier \".ZIP\" téléchargé  ${neutre}"
			rm -f $raspbian_zip
			
			### Renommer le fichier extrait
			printf "${bleuclair}\n\t\t* Renommage du fichier extrait  ${neutre}"
			mv 20*raspbian*lite.img $ImageRaspbian
			
			### Saut de ligne
			printf "\n";;

		$CHOIX02)
			Texte="..::: $CHOIX02 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX02"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			### Par défaut c'est OK...
			if [ "$execCHOIX02" = "" ]; then
				execCHOIX02="OK" 
			fi
			
			### Liste des peripheriques "possibles"
			printf "${bleuclair}\n\t* Lister les peripheriques disponibles (liste filtrée)  ${neutre}"
			listDEV=($(lsblk | grep -i "Dis" | grep -v "nvme" | grep -v "sda" | grep -v "hd?" | cut -d" " -f1))
			printf ${gris} ${listDEV[@]} ${neutre}
			
			### Nombre de peripheriques
			nbDEV=${#listDEV[*]}
			
			### Capacite des peripheriques
			sizeDEV=($(lsblk | grep -i "Dis" | grep -v "nvme" | grep -v "sda" | grep -v "hd?" | cut -c23-29))
			
			### Menu de selection des peripheriques
			printf "${gris}\n\t\t$nbDEV périphérique(s) disponible(s) ${neutre}"
			menuDEV=();
			for (( i=0; i < $nbDEV; i++ )); do
   				menuDEV+=("${listDEV[$i]} ")
   				menuDEV+=("${sizeDEV[$i]} ")
   				printf "${gris}\n\t\t ${listDEV[$i]} : ${sizeDEV[$i]} ${neutre}"
			done 
			menuDEV+=("RETOUR"); menuDEV+=(": Retour au menu principal") # ligne pour retour au menu précédent
			
			### Question  : Choix de la destination ?
			SUPPORT=$(whiptail --title "Choix de la destination" --menu "\nQuel suppport pour RASPBIAN ?" 16 $largeurDialogBox 7 "${menuDEV[@]}" 3>&1 1>&2 2>&3)
			SUPPORT=` echo $SUPPORT| sed -e " s/\ //g" `
			exitstatus=$?
			
			if [ $exitstatus == 0 ] && ! [ "$SUPPORT" = "" ] && ! [ "$SUPPORT" = "RETOUR" ]; then
				printf "${bleuclair}\n\t* Support sélectionné : $SUPPORT ${neutre}"
				libCHOIX02="[OK] Support sélectionné : '$SUPPORT'"
			else
				printf "${bleuclair}\t\t* Annulation choix du support ! ${neutre}"
				execCHOIX02=""; libCHOIX02=$libOriginelChOIX02; SUPPORT=""
			fi	
			
			### Saut de ligne
			printf "\n";;
			
		$CHOIX03)
			Texte="..::: $CHOIX03 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX03"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			### Par défaut c'est OK...
				if [ "$execCHOIX03" = "" ]; then
					execCHOIX03="OK"; libCHOIX03="[OK] Copie effectuée" # on prépare le nouveau libellé de menu
				fi
			printf "${bleuclair}\n\t* Démontage des partitions ${neutre}"
			if [ "$SUPPORT" = "" ]; then
				### Message : Il faut d abord choisir le support !
				whiptail --title "Copie de RASPBIAN" --msgbox "Veuillez d'abord choisir un support." 10 $largeurDialogBox
				### Le choix n est pas possible on a donc rien executé ! 
				execCHOIX03=""; libCHOIX03=$libOriginelChOIX03
			elif [ ! -f "$ImageRaspbian" ]; then
				### Si le fichier $ImageRaspbian n'est pas détecté, il y a forcément un problème au niveau de l'étape 2...
				### Message : Soucis avec étape 2
				whiptail --title "Copie de RASPBIAN" --msgbox "Le fichier '$ImageRaspbian' est absent.\nVeuillez vérifier le bon déroulement des étapes précédentes." 10 $largeurDialogBox
				execCHOIX03=""; libCHOIX03=$libOriginelChOIX03
			else
				### Démontage préalable des partitions
				### Liste des partitions montées pour le "device" correspondant :
				listPART=($(cat /etc/mtab | grep "/dev/$SUPPORT" | cut -d' ' -f1))
				mountPART=($(cat /etc/mtab | grep "/dev/$SUPPORT" | cut -d' ' -f2))
				nbPART=${#listPART[@]}
				printf "${gris}\n\t\tListe des partitions à démonter ${neutre}"
				for (( i=0; i < $nbPART; i++ )); do 
					printf "${gris}\n\t\t\t${listPART[$i]} montée sur ${mountPART[$i]} ${neutre}"
				done
				
				printf "${gris}\n\t\tDémontage des partitions liées à $SUPPORT ${neutre}"
				### Démonter les partitions utilisées...
				for (( i=0; i < $nbPART; i++ )); do 
					printf "${gris}\n\t\t\tDémontage de ${listPART[$i]} montée sur ${mountPART[$i]} ${neutre}"
					umount -f ${listPART[$i]}
				done 

				### OK installer RASPBIAN sur le support
				printf "${bleuclair}\n\t* Copie de RASPBIAN sur $SUPPORT ${neutre}"
				### Etape préalable : disposer de l'utilitaire "pv"
				printf "${gris}\n\t\tInstallation de l'utilitaire 'pv', si nécessaire  $SUPPORT ${neutre}"
				apt -y install pv >/dev/null 
				
				### Rappel de la commande habituelle : dd bs=1M if="$ImageRaspbian" of=/dev/$SUPPORT status=progress conv=fsync oflag=nocache,sync
				SIZEDD=$(sudo parted -m "$ImageRaspbian" unit B print | grep ext4 | cut -d: -f3 | cut -dB -f1)
				printf "${gris}\n\t\tCopie de '$ImageRaspbian' sur '/dev/$SUPPORT' ${neutre}"
    			(sudo dd if="$ImageRaspbian" bs=1M | pv -n --size $SIZEDD | sudo dd of="/dev/$SUPPORT" bs=1M oflag=nocache,sync) 2>&1 | whiptail --gauge "Veuillez patienter pendant la copie de RASPBIAN...\n...vers le périphérique [$SUPPORT]..." 6 $largeurDialogBox 0
    			
    			### Détacher le périphérique
				printf "${bleuclair}\n\t* Détacher le périphérique $SUPPORT ${neutre}"
				udisksctl power-off -b /dev/$SUPPORT 2>&1
				
				### Message suggérant de retirer physiquement le périphérique $SUPPORT				
				whiptail --title "Copie de RASPBIAN" \
					--msgbox "La copie est terminée.\nVeuillez débrancher puis rebrancher votre périphérique '$SUPPORT'.\n\nMerci de ne cliquer sur 'OK' que lorsque c'est fait." \
					12 $largeurDialogBox
			fi
			
			### Saut de ligne
			printf "\n";;

		$CHOIX04)
			Texte="..::: $CHOIX04 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX04"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			### Par défaut c'est OK...			
			if [ "$execCHOIX04" = "" ]; then
				execCHOIX04="OK"
			fi
			
			if [ "$SUPPORT" = "" ]; then
				### Message : Il faut d abord choisir le support !
				printf "${gris}\n\t\tAucun périphérique défini ! ${neutre}"
				whiptail --title "$titreBOX" --msgbox "Veuillez d'abord choisir un support." 10 $largeurDialogBox
				### Le choix n est pas possible on a donc rien executé ! 
				execCHOIX04=""; libCHOIX04=$libOriginelChOIX04
			else
				### OK le support existe
				### Etape préalable : les partitions doivent être montées/détectées
				printf "${gris}\n\t\tListe des partitions nécessaires liée à $SUPPORT :${neutre}"
				PointMontageRootFS="" # initialisation de la variable
				PointMontageRootFS=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "rootfs" | cut -d' ' -f2)
				PartitionRootFS=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "rootfs" | cut -d' ' -f1)
				#PointMontageBoot=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "boot" | cut -d' ' -f2)
				#printf "${gris}\n\t\t\t$PointMontageBoot :${neutre}"
				printf "${gris}\n\t\t\t$PointMontageRootFS :${neutre}"
				
				if [ "$PointMontageRootFS" = "" ]; then
					### Message : Problème, le point de montage est vide
					printf "${rougeclair}\n\t\tErreur : point de montage vide !\n${neutre}"
					whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : point de montage vide.\nLe support semble inacessible.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
					### On a rien executé, donc il faut ajuster les variables de "retour" 
					execCHOIX04=""; libCHOIX04=$libOriginelChOIX04	
				else
					### OK, point de montage non vide, on tente d'accéder au fichier "hostaname"
					fpHostname="$PointMontageRootFS/etc/hostname" # chemin complet vers hostname du RPI
					
					if [ ! -f $fpHostname ]; then
						### Message : Problème, le fichier est inaccessible
						printf "${rougeclair}\n\t\tErreur : fichier inaccessible !\n${neutre}"
						whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : fichier non trouvé.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
						### On a rien executé, donc il faut ajuster les variables de "retour" 
						execCHOIX04=""; libCHOIX04=$libOriginelChOIX04	
					else					
						### OK le fichier existe, on tente de lire son contenu
						NomActuelRPI="" # initialisation de la variable	
						NomActuelRPI=$(cat $fpHostname | head -1) # on récupère le nom actuellement inscrit dans le système Raspbian du RaspberryPi
						printf "${gris}\n\t\tNom actuellement incrit dans le système Raspbian : '$NomActuelRPI' ${neutre}"
						
						### Afficher le nom lu
						NomRPI=$(whiptail --title "$titreBOX" --inputbox "Nouveau nom pour votre RaspberryPI ?\n" 10 $largeurDialogBox $NomActuelRPI 3>&1 1>&2 2>&3)
						retourBOX=$?
						if [ $retourBOX = 0 ] && [ ! "$NomRPI" = "" ]; then
							### Changement OK
						   printf "${gris}\n\t\tNouveau nom pour le système Raspbian : '$NomRPI' ${neutre}"
						   echo $NomRPI > "hostname" && mv -f "hostname" $fpHostname 
						   if [ ! $NomRPI = $(cat $fpHostname | head -1) ]; then
						   	printf "${rougeclair}\n\t\t\tEchec du (re)nommage ! ${neutre}"	
						   	whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : échec du (re)nommage.\n" 10 $largeurDialogBox
						   	### On a rien executé, donc il faut ajuster les variables de "retour" 
								execCHOIX04=""; libCHOIX04=$libOriginelChOIX04	
						   fi
						   ### OK 
						   ### On ajuste le fichier /etc/hosts (remplace "raspberry" par $NomRPI
						   sed -i -e "s/\/raspberry/\/$NomRPI/g" /etc/hosts
						   libCHOIX04="[OK] Nom inscrit : '$NomRPI'"
						else
							### Annulation
						   printf "${gris}\n\t\tNommage annulé ! ${neutre}"
						   ### On a rien executé, donc il faut ajuster les variables de "retour" 
							execCHOIX04=""; libCHOIX04=$libOriginelChOIX04	
						fi		
					fi		
				fi 
			fi

			printf "\n";;
			
		$CHOIX05)
			Texte="..::: $CHOIX05 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX05"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			if [ "$execCHOIX05" = "" ]; then
				execCHOIX05="OK"; libCHOIX05="[OK] $libCHOIX05"
			fi

			if [ "$SUPPORT" = "" ]; then
				### Message : Il faut d abord choisir le support !
				printf "${gris}\n\t\tAucun périphérique défini ! ${neutre}"
				whiptail --title "$titreBOX" --msgbox "Veuillez d'abord choisir un support." 10 $largeurDialogBox
				### Le choix n est pas possible on a donc rien executé ! 
				execCHOIX05=""; libCHOIX05=$libOriginelChOIX05
			else
				### OK le support existe
				### Etape préalable : les partitions doivent être montées/détectées
				printf "${gris}\n\t\tListe des partitions nécessaires liée à $SUPPORT :${neutre}"
				PointMontageBoot="" # initialisation de la variable
				PointMontageBoot=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "boot" | cut -d' ' -f2)
				PartitionBoot=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "boot" | cut -d' ' -f1)
				printf "${gris}\n\t\t\t$PointMontageBoot :${neutre}"
				
				if [ "$PointMontageBoot" = "" ]; then
					### Message : Problème, le point de montage est vide
					printf "${rougeclair}\n\t\tErreur : point de montage vide !\n${neutre}"
					whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : point de montage vide.\nLe support semble inacessible.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
					### On a rien executé, donc il faut ajuster les variables de "retour" 
					execCHOIX05=""; libCHOIX05=$libOriginelChOIX05	
				else
					### OK, point de montage non vide, on tente de créer le fichier "ssh"
					fpSSH="$PointMontageBoot/ssh"
					touch $fpSSH >/dev/null
					### Tester l'existence du fichier : 
					if [ ! -f "$fpSSH" ]; then
						### Soucis le fichier n'existe pas !
						printf "${rougeclair}\n\t\tErreur : le fichier $fpSSH est absent !\n${neutre}"
						whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : point de montage vide.\nLe support semble inacessible.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
						### On a rien executé, donc il faut ajuster les variables de "retour" 
						execCHOIX05=""; libCHOIX05=$libOriginelChOIX05
					else
						printf "${gris}\n\t\tSSH activé sur '$SUPPORT' via '$fpSSH'.\n${neutre}"
						whiptail --title "$titreBOX" --msgbox "SSH activé." 10 $largeurDialogBox
					fi
				fi
			fi
			printf "\n";;
			
		$CHOIX06)
			Texte="..::: $CHOIX06 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX06"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			if [ "$execCHOIX06" = "" ]; then
				execCHOIX06="OK"; libCHOIX06="[OK] $libCHOIX06"
			fi
			
			### Test si support sélectionné
			if [ "$SUPPORT" = "" ]; then
				### Message : Il faut d abord choisir le support !
				printf "${gris}\n\t\tAucun périphérique défini ! ${neutre}"
				whiptail --title "$titreBOX" --msgbox "Veuillez d'abord choisir un support." 10 $largeurDialogBox
				### Le choix n est pas possible on a donc rien executé ! 
				execCHOIX06=""; libCHOIX06=$libOriginelChOIX06
			else
				### OK le support existe
				### Etape préalable : les partitions doivent être montées/détectées
				printf "${gris}\n\t\tListe des partitions nécessaires liée à $SUPPORT :${neutre}"
				PointMontageRootFS="" # initialisation de la variable
				PointMontageRootFS=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "rootfs" | cut -d' ' -f2)
				PartitionRootFS=$(cat /etc/mtab | grep "/dev/$SUPPORT" | grep -i "rootfs" | cut -d' ' -f1)

				printf "${gris}\n\t\t\t$PointMontageRootFS :${neutre}"
				
				if [ "$PointMontageRootFS" = "" ]; then
					### Message : Problème, le point de montage est vide
					printf "${rougeclair}\n\t\tErreur : point de montage vide !\n${neutre}"
					whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : point de montage vide.\nLe support semble inacessible.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
					### On a rien executé, donc il faut ajuster les variables de "retour" 
					execCHOIX06=""; libCHOIX06=$libOriginelChOIX06	
				else
					### OK, point de montage non vide, on tente d'accéder au fichier "$fpWIFI"
					fpWIFI="$PointMontageRootFS/etc/wpa_supplicant/wpa_supplicant.conf" # chemin complet vers le fichier de configuration du WIFI
					
					if [ ! -f $fpWIFI ]; then
						### Message : Problème, le fichier est inaccessible
						printf "${rougeclair}\n\t\tErreur : fichier '$fpWIFI' inaccessible !\n${neutre}"
						whiptail --title "$titreBOX" --msgbox "Une erreur est survenue : \nFichier '$fpWIFI' non trouvé.\nVeuillez vérifier le branchement." 10 $largeurDialogBox
						### On a rien executé, donc il faut ajuster les variables de "retour" 
						execCHOIX06=""; libCHOIX06=$libOriginelChOIX06	
					else					
						### OK le fichier existe.
						###================================================================================================================================
						### SOLUTION DE FACILITE :
						### L'objectif étant d'inscrire la configuration WIFI pour un Raspberry fraichement installé,
						### aucun test ne sera fait pour savoir si une configuration WIFI existante déjà, on va ECRASER le fichier existant !
						###================================================================================================================================
						
						### Demander les différentes informations
						WifiSSID=$(whiptail --title "$titreBOX" --inputbox "Veuillez entrer le SSID :\n" 10 $largeurDialogBox 3>&1 1>&2 2>&3)
						retourBOX=$?
						if [ $retourBOX = 0 ]; then
							WifiPWD=$(whiptail --title "$titreBOX" --passwordbox "Veuillez entrer le mot de passe :\n" 10 $largeurDialogBox 3>&1 1>&2 2>&3)
							retourBOX=$?
						fi
						
						if [ $retourBOX = 0 ] && [ ! "$WifiSSID" = "" ] && [ ! "$WifiPWD" = "" ]; then
							### OK on tenter de pousser tout ça vers le fichier $fpWIFI
						   printf "${gris}\n\t\tInscription du paramétrage WIFI sur '$SUPPORT' ${neutre}"
							
							fpWIFIlocal="wpa_supplicant.conf"
							LigneWIFI="### Pré-configuration du Wifi"; echo -e $LigneWIFI > $fpWIFIlocal
							LigneWIFI="ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="update_config=1"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="country=FR"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="network={"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="\tssid='$WifiSSID'"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="\tpsk='$WifiPWD'"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="\tkey_mgmt=WPA-PSK"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="\tscan_ssid=1"; echo -e $LigneWIFI >> $fpWIFIlocal
							LigneWIFI="}"; echo -e $LigneWIFI >> $fpWIFIlocal					   

							### Le fichier $fpWIFIlocal est complet, on tente de le transférer vers sa "vraie" destination
						   mv -f "$fpWIFIlocal" "$fpWIFI" 
						   
						   ### OK 
						   libCHOIX06="[OK] Configuration WIFI effectuée."
						else
							### Annulation
						   printf "${gris}\n\t\tConfiguration WIFI annulée ! ${neutre}"
						   ### On a rien executé, donc il faut ajuster les variables de "retour" 
							execCHOIX06=""; libCHOIX06=$libOriginelChOIX06	
						fi		
					fi		
				fi 
			fi			
			
			printf "\n";;
			
		$CHOIX07)
			Texte="..::: $CHOIX07 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX07"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			if [ "$execCHOIX07" = "" ]; then
				execCHOIX07="OK"; libCHOIX07="[OK] $libCHOIX07"
			fi
			Texte="Téléchargement du dernier script d'installation des fonctionnalités"; Niveau=2; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			fichierWEB="install.sh"; wget -O "$fichierWEB" -q --show-progress $url_fic_install
			PartitionMontee "rootfs" "$SUPPORT" "PointMontage" "$titreBOX"
			if [ ! "$PointMontage" = "" ]; then
				fpLocal="$PointMontage/$fichierWEB" # chemin complet vers le fichier
				Texte="Déplacer le fichier téléchargé vers '$fpLocal'"; Niveau=3; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur"; 
				mv "$fichierWEB" "$fpLocal"
				if [ $? = 0 ]; then
					whiptail --title "$titreBOX" --msgbox "Fichier déplacé vers '$fpLocal'." 10 $largeurDialogBox
				else
					whiptail --title "$titreBOX" --msgbox "Echec de l'opération." 10 $largeurDialogBox
					### ECHEC, donc il faut ajuster les variables de "retour" 
					execCHOIX07=""; libCHOIX07=$libOriginelChOIX07	
				fi
			else
				whiptail --title "$titreBOX" --msgbox "La copie du fichier a échouée." 10 $largeurDialogBox
				Texte="La copie du fichier a échouée."; Niveau=3; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
				### ECHEC, donc il faut ajuster les variables de "retour" 
				execCHOIX07=""; libCHOIX07=$libOriginelChOIX07	
			fi
			printf "\n";;
					
		$CHOIX08)
			Texte="..::: $CHOIX08 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			titreBOX="$libOriginelChOIX08"
			printf "${bleuclair}\n\t*$titreBOX : ${neutre}"
			### Par défaut c'est OK...
				if [ "$execCHOIX08" = "" ]; then
					execCHOIX08="OK"; libCHOIX08="[OK] $libOriginelChOIX08" # on prépare le nouveau libellé de menu
				fi
			printf "${bleuclair}\n\t* Démontage des partitions ${neutre}"
			if [ "$SUPPORT" = "" ]; then
				### Message : Il faut d abord choisir le support !
				whiptail --title "$titreBOX" --msgbox "Veuillez d'abord choisir un support." 10 $largeurDialogBox
				### Le choix n est pas possible on a donc rien executé ! 
				execCHOIX07=""; libCHOIX07=$libOriginelChOIX07
			else
				### Démontage des partitions
				### Liste des partitions montées pour le "device" correspondant :
				listPART=($(cat /etc/mtab | grep "/dev/$SUPPORT" | cut -d' ' -f1))
				mountPART=($(cat /etc/mtab | grep "/dev/$SUPPORT" | cut -d' ' -f2))
				nbPART=${#listPART[@]}
				printf "${gris}\n\t\tListe des partitions à démonter ${neutre}"
				for (( i=0; i < $nbPART; i++ )); do 
					printf "${gris}\n\t\t\t${listPART[$i]} montée sur ${mountPART[$i]} ${neutre}"
				done
				
				printf "${gris}\n\t\tDémontage des partitions liées à $SUPPORT ${neutre}"
				### Démonter les partitions utilisées...
				for (( i=0; i < $nbPART; i++ )); do 
					printf "${gris}\n\t\t\tDémontage de ${listPART[$i]} montée sur ${mountPART[$i]} ${neutre}"
					umount -f ${listPART[$i]}
				done 
    			
    			### Détacher le périphérique
				printf "${bleuclair}\n\t* Détacher le périphérique $SUPPORT ${neutre}"
				udisksctl power-off -b /dev/$SUPPORT 2>&1
				
				### Message suggérant de retirer physiquement le périphérique $SUPPORT				
				whiptail --title "$titreBOX" \
					--msgbox "Vous pouvez retirer le périphérique '$SUPPORT'." \
					12 $largeurDialogBox
			fi
			
			### Saut de ligne
			printf "\n";;		
			
		$CHOIX09)
			### =======================================
			### QUITTER l assistant
			Texte="..::: $CHOIX09 :::.. "; Niveau=1; RetourLigne=1; MsgErreur=0; MsgConsole "$Texte" "$Niveau" "$RetourLigne" "$MsgErreur";
			if [ "$execCHOIX08" = "" ]; then
				execCHOIX08="OK"; libCHOIX08="[OK] $libCHOIX08"
			fi
			menuprincipal=false
			printf "\n";;

		*)
		### Option non gérée ? Quitter le menu !
		menuprincipal=false
		
		### Saut de ligne
		printf "\n";;
	esac
done

#echo "Exit status = " $exitstatus " / Choix = " $CHOIX

### Sortie 
if [ "$menuprincipal" = false ]; then
	printf "${jauneclair}\nAu revoir.\n${neutre}"
	exit 0
else
	exit 1
fi

